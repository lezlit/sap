import requests

r = requests.get('https://data.sensor.community/airrohr/v1/sensor/10001/')
data = r.json()[1]
timestamp = data['timestamp']
print('Timestamp: ', timestamp)
sensor_values = data['sensordatavalues']

for value in sensor_values:
    if value['value_type'] == 'P1':
        print('P10:', value['value'])
    if value['value_type'] == 'P2':
        print('P2.5:', value['value'])
