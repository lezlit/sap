#!/usr/bin/env bash


api_url='https://data.sensor.community/airrohr/v1/sensor/10001/'
json_data="$(curl -s "$api_url" )"

timestamp="$(jq -r '.[1].timestamp' <<< "$json_data")"
p1_p2="$(jq -r '.[1].sensordatavalues[] | "\(.value_type): \(.value)"' <<< $json_data | sed -e 's/P1/P10/g' -e 's/P2/P2.5/g')"

printf 'Timestamp: %s\n%s\n' "$timestamp" "$p1_p2"
